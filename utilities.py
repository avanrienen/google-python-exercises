#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

'''
Some examples re Utilities
See https://developers.google.com/edu/python/utilities
'''

import sys
import os
import commands
import urllib


## Example pulls filenames from a dir, prints their relative and absolute paths
def printdir(dir):
    filenames = os.listdir(dir)

    # remove filename to ignore
    filenames = ignore_filenames(filenames)

    for filename in filenames:
        #print filename  # foo.txt
        #print os.path.join(dir, filename)  # dir/foo.txt (relative to current dir)
        print os.path.abspath(os.path.join(dir, filename))  # /home/nick/dir/foo.txt


def read_file(filename):
    '''
    '''

    try:
        ## Either of these two lines could throw an IOError, say
        ## if the file does not exist or the read() encounters a low level error.
        f = open(filename, 'rU')
        text = f.read()
        print text
        f.close()
    except IOError:
        ## Control jumps directly to here if any of the above lines throws IOError.
        sys.stderr.write('Could not read file: ' + filename)
    ## In any case, the code then continues with the line after the try/except


def ignore_filenames(filenames):
    ''' (list) -> list

    Removes filenames that should be ignored

    >>> ignore_filenames(['$RECYCLE.BIN'])
    ['']
    '''
    # todo: does not work (strange)
    #print filenames
    # iterate thru list of filenames
    for filename in filenames:

        if filename == '$RECYCLE.BIN':
            filenames.remove(filename)
        if filename == '.localized':
            filenames.remove(filename)
        if filename == 'desktop.ini':
            filenames.remove(filename)
        if filename == 'tmp':
            filenames.remove(filename)
        if filename == '.DS_Store':
            filenames.remove(filename)
    #print filenames

    return filenames


## Given a url, try to retrieve it. If it's text/html,
## print its base url and its text.
def wget(url):
    ufile = urllib.urlopen(url)  # get file-like object for url
    info = ufile.info()  # meta-info about the url content
    if info.gettype() == 'text/html':
        print 'base url:' + ufile.geturl()
        text = ufile.read()  # read all its text
        print text


## Version that uses try/except to print an error message if the
## urlopen() fails.
def wget2(url):
    try:
        ufile = urllib.urlopen(url)
        if ufile.info().gettype() == 'text/html':
            print ufile.read()
    except IOError:
        print 'problem reading url:', url


## Given a dir path, run an external 'ls -l' on it --
## shows how to call an external program
def listdir(dir):
    cmd = 'ls -l ' + dir
    print "Command to run:", cmd  # good to debug cmd before actually running it
    (status, output) = commands.getstatusoutput(cmd)
    if status:  # Error case, print the command's output to stderr and exit
        sys.stderr.write(output)
        sys.exit(1)
    print output  # Otherwise do something with the command's output


# Define a main() function that prints a little greeting.
def main():
    url = ''
    #printdir("/Users/avanrienen/Downloads")

    #read_file('test.txt')

    #wget('http://www.tagesschau.de/')
    #wget('http://www.ssa.gov/oact/babynames/decades/names2000s.html')

    # one
    url = 'www.scandio.de'
    print url
    wget2(url)
    # two
    url = 'http://www.scandio.de'
    print url
    wget2(url)


# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
    main()
