#!/usr/bin/python2.4 -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

# Additional basic string exercises

# D. verbing
# Given a string, if its length is at least 3,
# add 'ing' to its end.
# Unless it already ends in 'ing', in which case
# add 'ly' instead.
# If the string length is less than 3, leave it unchanged.
# Return the resulting string.
def verbing(s):
  if len(s) >= 3:
    if s[-3:] == 'ing':
      s = s + 'ly'
    else:
      s = s + 'ing'
  return s


# E. not_bad
# Given a string, find the first appearance of the
# substring 'not' and 'bad'. If the 'bad' follows
# the 'not', replace the whole 'not'...'bad' substring
# with 'good'.
# Return the resulting string.
# So 'This dinner is not that bad!' yields:
# This dinner is good!
def not_bad(s):
  # Define strings to search for
  str1 = 'not'
  str2 = 'bad'
  # Define alternative string
  str3 = 'good'

  # Determine length of both input strings
  lenString1 = len(str1)
  lenString2 = len(str2)

  # Determine index (=position) of both input strings
  indexString1 = s.find(str1)
  indexString2 = s.find(str2)


  # Input equals output ...
  strOut = s

  # ... except a word substitution is required
  # In case string2 follows string1
  if indexString2 > indexString1:
    strOut = s[0:indexString1] + str3 + s[indexString2 + lenString2:]

  return strOut


# F. front_back
# Consider dividing a string into two halves.
# If the length is even, the front and back halves are the same length.
# If the length is odd, we'll say that the extra char goes in the front half.
# e.g. 'abcde', the front half is 'abc', the back half 'de'.
# Given 2 strings, a and b, return a string of the form
#  a-front + b-front + a-back + b-back
def front_back(a, b):

  # String A: odd or even?
  slicedStrings = slice_string(a)
  strAFront = slicedStrings[0]
  strABack = slicedStrings[1]

  # String B: odd or even?
  slicedStrings = slice_string(b)
  strBFront = slicedStrings[0]
  strBBack = slicedStrings[1]

  # Return a string of the form
  #  a-front + b-front + a-back + b-back
  strOut = strAFront + strBFront + strABack + strBBack

  return strOut


def slice_string(s):
  # Modulu 0: even string
  if len(s) % 2 == 0:
    # Split string in two halfs ...
    intSlice = len(s) / 2
  else: # Modulu = 1: odd string
    intSlice = len(s) / 2 + 1 # Extra char goes to front

  string_list = [] # define empty list

  # Determine front string ...
  string_list.append(s[0:intSlice])
  # ... and back string
  string_list.append(s[intSlice:])

  return string_list

# Simple provided test() function used in main() to print
# what each function returns vs. what it's supposed to return.
def test(got, expected):
  if got == expected:
    prefix = ' OK '
  else:
    prefix = '  X '
  print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


# main() calls the above functions with interesting inputs,
# using the above test() to check if the result is correct or not.
def main():
  print 'verbing'
  test(verbing('hail'), 'hailing')
  test(verbing('swiming'), 'swimingly')
  test(verbing('do'), 'do')

  print
  print 'not_bad'
  test(not_bad('This movie is not so bad'), 'This movie is good')
  test(not_bad('This dinner is not that bad!'), 'This dinner is good!')
  test(not_bad('This tea is not hot'), 'This tea is not hot')
  test(not_bad("It's bad yet not"), "It's bad yet not")

  print
  print 'front_back'
  test(front_back('abcd', 'xy'), 'abxcdy')
  test(front_back('abcde', 'xyz'), 'abcxydez')
  test(front_back('Kitten', 'Donut'), 'KitDontenut')

if __name__ == '__main__':
  main()
