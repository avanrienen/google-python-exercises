#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

"""Wordcount exercise
Google's Python class

The main() below is already defined and complete. It calls print_words()
and print_top() functions which you write.

1. For the --count flag, implement a print_words(filename) function that counts
how often each word appears in the text and prints:
word1 count1
word2 count2
...

Print the above list in order sorted by word (python will sort punctuation to
come before letters -- that's fine). Store all the words as lowercase,
so 'The' and 'the' count as the same word.

2. For the --topcount flag, implement a print_top(filename) which is similar
to print_words() but which prints just the top 20 most common words sorted
so the most common word is first, then the next most common, and so on.

Use str.split() (no arguments) to split on all whitespace.

Workflow: don't build the whole program at once. Get it to an intermediate
milestone and print your data structure and sys.exit(0).
When that's working, try for the next milestone.

Optional: define a helper function to avoid code duplication inside
print_words() and print_top().

"""
# Define print_words(filename) and print_top(filename) functions.
# You could write a helper utility function that reads a file
# and builds and returns a word/count dict for it.
# Then print_words() and print_top() can just call the utility function.

import sys

glob_reporting = True  # ... toggle reporting
global_word_count = 0  # ... total word count
# stop words
g_path_stop_words = "../stop-words-collection-2014-02-24/stop-words/"
# English
g_file_stop_words_uk_1 = g_path_stop_words + "stop-words_english_1_en.txt"
g_file_stop_words_uk_2 = g_path_stop_words + "stop-words_english_2_en.txt"
g_file_stop_words_uk_3 = g_path_stop_words + "stop-words_english_3_en.txt"
# German
g_file_stop_words_de_1 = g_path_stop_words + "stop-words_german_1_de.txt"
g_file_stop_words_test = g_path_stop_words + "test-avr.txt"
# Define threshold to ignore words below a certain count
g_word_threshold = 3


def print_words(file):
    ''' (file, boolean) >>> list of (string string)

    Prints a list of words
    counting how often each word appears in a given text file

    >>> print_words(small.txt)
    word1:  count1
    word2:  count2
    '''
    # define empty dictionary to return
    dic = {}
    # get dictionary based on given file
    dic = get_dict(file)

    # apply threshold
    if g_word_threshold > 0:
        dic = apply_threshold(dic)

    # iterate over dict and build output list
    for item in sorted(dic.keys()):
        print item, dic[item]
    # print total word count
    if glob_reporting:
        print_file_stats(file)


def apply_threshold(dic):
    ''' (dic) -> dic

    Remove all words,
     for which the word count is below the given threshold
    '''

    global global_word_count

    # create new dictionary
    new_dic = {}

    # iterate thru dic and verify word count
    for item in dic:
        # retrieve count ...
        if dic[item] > g_word_threshold:
            # add item to new dic only if word count is above threshold
            new_dic[item] = dic[item]
            # reduce word counter by -1
            global_word_count -= 1

    # compare old and new dic
    #print len(dic)
    #print len(new_dic)

    return new_dic


def print_file_stats(file):
    ''' (file, int) >>> text

    Prints some stats about analysed file

    '''
    print ""
    print "  -----"
    print "  " + file
    print "  Words: " + str(global_word_count)
    # print threshold, if > 0
    if g_word_threshold:
        print "  Threshold: " + str(g_word_threshold)
    print ""


def print_top(file):
    ''' (file) -> list of (string string)

    Prints a list of the 20 most often appearing words
    in a given text file

    >>> print_words(small.txt)
    word1:  count1
    ...
    word20:  count20
    '''
    # define empty output list
    list_words = []

    # define empty dictionary to return
    dic = {}

    if glob_reporting:
        print ""
        print " >> Creating dictionary ..."

    # get dictionary based on given file
    dic = get_dict(file)

    if glob_reporting:
        print " >> Done."

    if glob_reporting:
        print " >> Retrieving word statistics ..."

    # iterate over dict and build output list
    for item in sorted(dic.keys()):
        list_words.append([item, dic[item]])
        list_words = sort_count(list_words, bol_reverse=True)

    if glob_reporting:
        print " >> Done."

    # count how many results shown
    count = 0
    count_max = 20

    # in case less than (count_max) words are in list
    if len(list_words) < count_max:
        count_max = len(list_words)

    if glob_reporting:
        print " >> Here you go:"
        print ""

    # iterate over and print output
    while count < count_max:
        # output either in dict format ...
        #print list_words[count]
        # ... or defined list format
        print list_words[count][0] + ':  ' + str(list_words[count][1])
        # increment count
        count += 1

    if glob_reporting:
        print ""


# sort list by count of words
def sort_count(list, bol_reverse=False):
    return sorted(list, key=last_list_element, reverse=bol_reverse)


# returns the last element of a list
def last_list_element(list):
    return list[-1]


def get_dict(file):
    ''' (file) -> dict of (string: integer)

    Reads a text file and builds and returns a word/count dict

    >>> get_dict(small.txt)
    word1 count1
    word2 count2
    '''

    global global_word_count

    # define empty dictionary to return
    final_dict = {}

    # get content of file as string
    text = get_file(file)

    # split words by whitespace
    words = text.split()

    # remove stop words 1, 2, and 3
    words = remove_words(words, get_file(g_file_stop_words_uk_1))
    words = remove_words(words, get_file(g_file_stop_words_uk_2))
    words = remove_words(words, get_file(g_file_stop_words_uk_3))
    # remove stop words from private list
    words = remove_words(words, get_file(g_file_stop_words_test))

    # iterate over words
    for w in words:
        # add word (with count = 1) to dict for new words ...

        # do not add empty strings
        if len(w) > 0:
            # increment word count
            global_word_count += 1
            if w not in final_dict:
                count = 1
                final_dict[w] = count
            # ... otherwise, determine current count ...
            else:
                # retrieve current count ...
                count = final_dict[w]
                # ... and increase it by one
                final_dict[w] = count + 1

    # return (unsorted) dictionary
    return final_dict


def remove_words(words, words_tb_removed):
    ''' (list) -> list

    Removes defined stops words from a given list

    >>> remove_words(['a', 'you', 'hello'])
    ['hello']
    >>> remove_words(['this', 'is', 'an', 'awesame', 'new', 'day'])
    ['awesame', 'day']
    '''

    # define variables
    cleaned_words = []
    temp_list = []

    # iterate through list of words ...
    for w in words:
        # split strings which are "glued" by '--'
        temp_list = w.split('--')
        # continue to count only if '--' was found
        if len(temp_list) > 1:
            words += temp_list
        else:
            # Trim unwanted characters to clean-up words
            w = remove_punctuation(w.lower())
            # ... and verify if it is a stop word
            if not w in words_tb_removed:
                # only append it, if this is not the case
                cleaned_words.append(w)
    # return list of "non stop words"
    return cleaned_words


def get_file(file):
    ''' (file) -> string

    Opens a file and returns the complete content as a string

    >>> get_file(alice.txt)
    Alice's Adventures in Wonderland
    '''

    # create variables
    text = ''

    # read text file
    file = open(file, 'r')
    # read whole file at once and populate string
    text = file.read()
    # close file
    file.close()

    return text


def remove_punctuation(word):
    ''' (sring) -> string

    Verifies whether one or more special characters ("punctuation")
    precedes or follows a given string and removes it or them

    >>> remove_punctuation("hello,")
    hello
    >>> remove_punctuation("nothing--")
    nothing
    '''

    # restrict output to words starting with ...
    #if not word.startswith('e'):
    #    return ''

    # create empty variables
    chars_to_trim = []
    cleaned_word = ''

    # get list of characters to trim from string
    # todo: switch to external text file to maintain list of chars
    chars_to_trim = ['\'', '\"', '`', '.', ',', '?', '!', '-', ';', ':', '(', ')', '%', '{', '}', '[', ']', '|']

    # set output to input string before ...
    cleaned_word = word
    # ... trimming unwanted characters ...
    # ... by iterating over every character of the word ...
    for c in cleaned_word:
        # ... and comparing it to the set of "unwanted" characters ...
        for char in chars_to_trim:
            # ... first from the start ...
            while cleaned_word.startswith(char):
                cleaned_word = cleaned_word[1:]
            # ... and then from the end of the word
            while cleaned_word.endswith(char):
                cleaned_word = cleaned_word[:-1]

    return cleaned_word


###
# This basic command line argument parsing code is provided and
# calls the print_words() and print_top() functions which you must define.
def main():

    if len(sys.argv) != 3:
        print 'usage: ./wordcount.py {--count | --topcount} file'
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)
    else:
        print 'unknown option: ' + option
        sys.exit(1)


if __name__ == '__main__':
    main()
