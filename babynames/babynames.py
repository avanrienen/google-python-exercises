#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re

"""Baby Names exercise

Define the extract_names() function below and change main()
to call it.

For writing regex, it's nice to include a copy of the target
text for inspiration.

Here's what the html looks like in the baby.html files:
...
<h3 align="center">Popularity in 1990</h3>
....
<tr align="right"><td>1</td><td>Michael</td><td>Jessica</td>
<tr align="right"><td>2</td><td>Christopher</td><td>Ashley</td>
<tr align="right"><td>3</td><td>Matthew</td><td>Brittany</td>
...

Suggested milestones for incremental development:
 -Extract the year and print it
 -Extract the names and rank numbers and just print them
 -Get the names data into a dict and print it
 -Build the [year, 'name rank', ... ] list and print it
 -Fix main() to use the extract_names list
"""


def extract_names(filename):
    ''' (filename) -> list

    Given a file name for baby.html, returns a list starting with the year string followed by the name-rank strings in alphabetical order.

    >>> extract_names(baby1990.html)
    ['1990', 'Aaron 34', 'Abbey 482', ...]
    '''

    # set variables
    text = ''
    names_dict = {}

    # get content of file as string
    text = get_file(filename)

    # define regular expression
    #  for:  <h3 align="center">Popularity in 1990</h3>
    match = re.search(r'Popularity in \d\d\d\d', text)

    # find year to extract
    if match:
        year = match.group()[-4:]
    else:
        print 'No year found.'

    # define regular expression
    #  for:  <tr align="right"><td>1</td><td>Michael</td><td>Jessica</td>
    # find names to extract
    names = re.findall(r'(<td>\d+</td>)(<td>\w+</td>)(<td>\w+</td>)', text)

    # if match was found
    if names:
        # iterave over each extracted name
        for name in names:
            # get rid of html tags
            #  by ommiting first and last four chars
            rank = name[0][4:-5]
            # extract name without html tags
            key1 = name[1][4:-5]
            key2 = name[2][4:-5]
            # add names to dict
            names_dict[key1] = rank
            names_dict[key2] = rank
    else:
        print 'Empty match'

    # transform dict to list
    names_list = build_names_list(year, names_dict)

    return names_list


def build_names_list(year, dict):
    ''' (int, dict) -> list

    Build the [year, 'name rank', ... ] list and returns it

    >>> build_names_list(1990, {'Tanisha': '385', 'Corbin': '426'})
    ['1990', 'Corbin 426', 'Tanisha 385']
    >>> build_names_list(1990, {'Abbey': '482', 'Aaron': '34'})
    ['1990', 'Aaron 34', 'Abbey 482']
    '''

    # create empty list
    names_list = []

    # add year as first item
    names_list.append(year)

    # iterate over alphabetically sorted dict ...
    for item in sorted(dict.keys()):
        # ... and add name and rank to list
        names_list.append(item + ' ' + dict[item])

    # return finished list
    return names_list


def get_file(file):
    ''' (file) -> string

    Opens a file and returns the complete content as a string

    >>> get_file(alice.txt)
    Alice's Adventures in Wonderland
    '''

    # read text file
    file = open(file, 'r')
    # read whole file at once and populate string
    text = file.read()
    # close file
    file.close()

    return text


def write_summary(file, text):
    ''' (file, string) -> None

    Writes text into a file and saves it

    '''

    outf = open(file + '.summary', 'w')
    outf.write(text + '\n')
    outf.close()


def main():
    # This command-line parsing code is provided.
    # Make a list of command line arguments, omitting the [0] element
    # which is the script itself.
    args = sys.argv[1:]

    if not args:
        print 'usage: [--summaryfile] file [file ...]'
        sys.exit(1)

    # Notice the summary flag and remove it from args if it is present.
    summary = False
    if args[0] == '--summaryfile':
        summary = True
        del args[0]

    # For each filename, get the names, then either print the text output
    # or write it to a summary file
    for file in args:
        names = extract_names(file)

        # change names list to text
        text = '\n'.join(names)

        # if summary in required in text file
        if summary:
            write_summary(file, text)
        else:
            print text

if __name__ == '__main__':
    main()
